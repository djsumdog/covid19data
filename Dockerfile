FROM python:3

VOLUME ["/site"]

WORKDIR /src
RUN mkdir -p /src
COPY charts.py build chart.html main.css /src/
COPY images /src/images
RUN chmod 700 /src/build /src/charts.py

CMD /src/build
