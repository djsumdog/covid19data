#!/usr/bin/env python3
from pprint import pprint


class JSDate(object):

    def __init__(self, date: str):
        parts = date.split('/')
        self.month = int(parts[0]) - 1
        self.day = int(parts[1]) + 7
        self.year = 2020

    def __repr__(self):
        # Date Formatting
        # see https://developers.google.com/chart/interactive/docs/gallery/annotationchart
        # Yes we're outputting Javascript from Python
        # because I am a terrible terrible human being
        # who spent 20 years in industry and apparently
        # never learnt how to make a fucking graph programtically
        return f'new Date({self.year}, {self.month}, {self.day})'


if __name__ == '__main__':

    data_file = 'COVID-19/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv'

    headers = []
    rows = []
    max = {}
    with open(data_file, 'r') as fd:
        lines = fd.readlines()
        headers = lines[0].split(',')
        for r in lines[1:]:
            csv_row = r.strip().split(',')
            # merge regions
            # TODO: merge all China regions into one
            c_row = [f'{csv_row[0]}:{csv_row[1]}']

            c_row += csv_row[2:]
            rows.append(c_row)
            # current death toll by country
            max[c_row[0]] = c_row[-1]

    top_limit = 8

    sorted_max = sorted(max.items(), key=lambda x: int(x[1]))
    top_nations = [l[0] for l in sorted_max[-top_limit:]]

    fatal_graph_rows = []
    fatal_per_day_rows = []
    gi = 0
    nation_order = []

    time_col_length = len(rows[0]) - 10

    for i in range(time_col_length):
        data_col_x = i + 3  # first data field

        time_row = [0] * (len(top_nations) + 1)
        print(headers)
        time_row[0] = JSDate(headers[data_col_x + 1])
        fatal_per_day_row = [JSDate(headers[data_col_x + 1])] * (len(top_nations) + 1)
        gi += 1
        for n_idx, nation in enumerate(top_nations):
            nation_row = [e for e in rows if e[0] == nation][0]
            time_row[n_idx + 1] = float(nation_row[data_col_x])

            if data_col_x == 3:
                fatal_per_day_row[n_idx + 1] = 0
            else:
                fatal_per_day_row[n_idx + 1] = float(nation_row[data_col_x]) - float(nation_row[data_col_x - 1])

            data_col_x += 1
        fatal_graph_rows.append(time_row)
        fatal_per_day_rows.append(fatal_per_day_row)

    for n_idx, nation in enumerate(top_nations):
        nation_order.append(nation.strip(':'))

    with open('pychart.js', 'w') as p:
        p.write(f'fatal_nations = {nation_order};\n')
        p.write(f'fatal_data = {fatal_graph_rows};\n')
        p.write(f'fatal_per_day = {fatal_per_day_rows};\n')
